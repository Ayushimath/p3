const cardData = require('./p3');

const sortedcards = cardData.map((eachcard) => {
    eachcard["CVV"] = Math.floor(Math.random() * 999);
    return eachcard
})
    .map((eachcard) => {
        eachcard["Valid"] = true;
        return eachcard
    })
    .map((eachcard) => {
        let dateArray = (eachcard.issue_date).split("/");
        if (parseInt(dateArray[0]) < 3) {
            eachcard["Valid"] = false
        }
        return eachcard;
    })
    .sort((a, b) => {
        let dateArray1 = (a.issue_date).split("/");
        let dateArray2 = (b.issue_date).split("/");
        if (parseInt(dateArray1[0]) === parseInt(dateArray2[0])) {
            return parseInt(dateArray1[1]) > parseInt(dateArray2[1])
        } else {
            return parseInt(dateArray1[0]) > parseInt(dateArray2[0]);
        }
    });
console.log(sortedcards);