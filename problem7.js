const cardData = require('./p3');
const fs = require('fs');


const dataWmonth = cardData.map((eachcard) => {
    eachcard["CVV"] = Math.floor(Math.random() * 999);
    return eachcard
})
    .map((eachcard) => {
        eachcard["Valid"] = true;
        return eachcard
    })
    .map((eachcard) => {
        let dateArray = (eachcard.issue_date).split("/");
        if (parseInt(dateArray[0]) < 3) {
            eachcard["Valid"] = false
        }
        return eachcard;
    })
    .sort((a, b) => {
        let dateArray1 = (a.issue_date).split("/");
        let dateArray2 = (b.issue_date).split("/");
        if (parseInt(dateArray1[0]) === parseInt(dateArray2[0])) {
            return parseInt(dateArray1[1]) > parseInt(dateArray2[1])
        } else {
            return parseInt(dateArray1[0]) > parseInt(dateArray2[0]);
        }
    }).reduce((monthdata, eachcard) => {
        let dateArray = (eachcard.issue_date).split("/");
        if (dateArray[0] === '1') {
            if ('jan' in monthdata) {
                monthdata["jan"].push(eachcard);
            }
            else {
                monthdata["jan"] = []
                monthdata["jan"].push(eachcard);
            }
        } else if (dateArray[0] === '2') {
            if ("feb" in monthdata) {
                monthdata["feb"].push(eachcard);
            }
            else {
                monthdata["feb"] = []
                monthdata["feb"].push(eachcard);
            }

        } else if (dateArray[0] === '3') {
            if ("mar" in monthdata) {
                monthdata["mar"].push(eachcard);
            }
            else {
                monthdata["mar"] = []
                monthdata["mar"].push(eachcard);
            }

        } else if (dateArray[0] === '4') {
            if ("apr" in monthdata) {
                monthdata["apr"].push(eachcard);
            }
            else {
                monthdata["apr"] = []
                monthdata["apr"].push(eachcard);
            }

        } else if (dateArray[0] === '5') {
            if ("may" in monthdata) {
                monthdata["may"].push(eachcard);
            }
            else {
                monthdata["may"] = []
                monthdata["may"].push(eachcard);
            }

        } else if (dateArray[0] === '6') {
            if ("june" in monthdata) {
                monthdata["june"].push(eachcard);
            }
            else {
                monthdata["june"] = []
                monthdata["june"].push(eachcard);
            }

        } else if (dateArray[0] === '7') {
            if ("july" in monthdata) {
                monthdata["july"].push(eachcard);
            }
            else {
                monthdata["july"] = []
                monthdata["july"].push(eachcard);
            }

        } else if (dateArray[0] === '8') {
            if ("aug" in monthdata) {
                monthdata["aug"].push(eachcard);
            }
            else {
                monthdata["aug"] = []
                monthdata["aug"].push(eachcard);
            }

        } else if (dateArray[0] === '9') {
            if ("sep" in monthdata) {
                monthdata["sep"].push(eachcard);
            }
            else {
                monthdata["sep"] = []
                monthdata["sep"].push(eachcard);
            }

        } else if (dateArray[0] === '10') {
            if ("oct" in monthdata) {
                monthdata["oct"].push(eachcard);
            }
            else {
                monthdata["oct"] = []
                monthdata["oct"].push(eachcard);
            }

        } else if (dateArray[0] === '11') {
            if ("nov" in monthdata) {
                monthdata["nov"].push(eachcard);
            }
            else {
                monthdata["nov"] = []
                monthdata["nov"].push(eachcard);
            }

        } else if (dateArray[0] === '12') {
            if ("dec" in monthdata) {
                monthdata["dec"].push(eachcard);
            }
            else {
                monthdata["dec"] = []
                monthdata["dec"].push(eachcard);
            }

        }
        return monthdata

    }, {});

console.log(dataWmonth);
const finaldataWmonth = JSON.stringify(dataWmonth);
fs.writeFile('./output.json', finaldataWmonth, function (err) {
    if (err) {
        console.log(err);
    } else {
        console.log("The file was saved!");
    }
}); 
