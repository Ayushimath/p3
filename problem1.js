const cardData = require('./p3')

const goodcards = cardData.reduce((goodcards, eachCard) => {
    let a = eachCard.card_number
    let sum = parseInt(a[0]) + parseInt(a[2]) + parseInt(a[4])
        + parseInt(a[6]) + parseInt(a[8]) + parseInt(a[10]) + parseInt(a[12]) + parseInt(a[14])
    if (sum % 2 === 1) {
        goodcards.push(a);
    }
    return goodcards;

}, []);

console.log(goodcards);